USE aptunes_examen; 
DROP procedure IF EXISTS GetSongDuration;

DELIMITER $$
USE aptunes_examen$$
-- Schrijf een stored procedure, GetSongDuration met één IN parameter --
CREATE PROCEDURE GetSongDuration (IN bandId INT) 
SQL SECURITY INVOKER							   
BEGIN

DECLARE ok INT DEFAULT 0;
DECLARE songDuration TINYINT UNSIGNED;
DECLARE totalDuration INT DEFAULT(0); 

--  Je moet de som bepalen met behulp van een cursor --
DECLARE liedjes_cursor CURSOR FOR SELECT Lengte FROM Liedjes WHERE Bands_Id = bandId;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET ok = 1;

OPEN liedjes_cursor;
getAlbum: LOOP
    FETCH liedjes_cursor INTO songDuration;
    IF ok = 1 THEN
        LEAVE getAlbum;
	END IF;
    SET totalDuration = totalDuration + songDuration; 
   -- Als de totale lengte groter is dan 60, dan wordt ‘lange duurtijd’ getoond, in de andere gevallen ‘normale duurtijd’. Er is dus geen output parameter. --
    IF totalDuration > 60 THEN 
		SELECT 'Lange duurtijd' AS 'duurtijd';
    ELSE 
		SELECT 'Normale duurtijd' AS 'duurtijd';
    END IF;
END LOOP getAlbum;

CLOSE liedjes_cursor;
END$$

DELIMITER ;