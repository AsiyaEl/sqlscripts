USE aptunes_examen;

SELECT Albums.Titel AS 'Album', Bands.Naam AS 'Band' FROM AlbumReleases 
INNER JOIN Albums ON Albums.Id = AlbumReleases.Albums_Id
INNER JOIN Bands ON Bands.Id = AlbumReleases.Bands_Id
ORDER BY Albums.Titel;