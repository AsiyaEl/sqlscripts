USE aptunes_examen;
DROP procedure IF EXISTS PopulateLiedjesGenres;

DELIMITER $$
USE aptunes_examen$$ 
CREATE PROCEDURE PopulateLiedjesGenres () 
BEGIN
-- Variabelen declareren 
DECLARE numberOfLiedjes INT DEFAULT(0); -- Declareer vier variabelen van type INT met default waarde 0 --
DECLARE numberOfGenres INT DEFAULT(0);  
DECLARE randomLiedjeId INT DEFAULT(0);
DECLARE randomGenreId INT DEFAULT(0);
-- Pas variabele numberOfLiedjes aan naar aantal liedjes in onze database en numberOfGenres naar aantal genres. --

SELECT COUNT(*) INTO numberOfLiedjes FROM Liedjes; 
SELECT COUNT(*) INTO numberOfGenres FROM Genres;  
SET randomLiedjeId = FLOOR(RAND() * numberOfLiedjes) + 1;
SET randomGenreId = FLOOR(RAND() * numberOfGenres) + 1;

IF (randomGenreId, randomLiedjeId) NOT IN (SELECT * FROM LiedjesGenres) THEN
	INSERT INTO LiedjesGenres(Genres_Id, Liedjes_Id)
	VALUES (randomGenreId, randomLiedjeId);
END IF;
END$$

DELIMITER ;

CALL PopulateLiedjesGenres();