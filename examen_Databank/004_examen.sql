USE aptunes_examen;
SELECT Titel, Lengte FROM Liedjes 
WHERE Id IN 
(SELECT Liedjes_Id FROM LiedjesGenres 
INNER JOIN Genres ON Genres.Id = Genres_Id
WHERE Naam LIKE 'Electronic%') 
ORDER BY Titel;