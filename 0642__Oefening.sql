use aptunes;

delimiter $$
create procedure CleanupOldMemberships(in enddate date, out num integer)
begin
start transaction;  -- deze wordt toegepast om te voorkomen dat een andere gebruiker een lidmaatschap toevoegt in database en deze verwijdert wordt wegens de delete stap --
select count(*) into num 
from Lidmaatschappen
where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < enddate;
set sql_safe_updates = 0;
delete
from Lidmaatschappen
where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < enddate;
set sql_safe_updates = 1;
commit;
end$$
delimiter ;