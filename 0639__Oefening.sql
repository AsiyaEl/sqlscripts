USE modernWays;
   -- WE VOEGEN DE VOLGENDE WAARDE IN TABEL PERSONEN --
  INSERT INTO Personen (
   Voornaam, Familienaam, AanspreekTitel
)
VALUES (
   'Jean-Paul', 'Sartre',''
);
 



INSERT INTO Boeken (
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsdatum,
   Commentaar,
   Categorie,
   Personen_Id
)
VALUES (
   'De Woorden', 'Antwerpen', 'De Bezige Bij', '1961', '', 'Een zeer mooi boek', 'Roman',
   (SELECT Id FROM Personen WHERE
       Familienaam = 'Sartre' AND Voornaam = 'Jean-Paul' AND (SELECT Boeken.Personen_Id FROM Boeken Cross JOIN  Personen WHERE  Boeken.Personen_Id = Personen.Id )));
       

