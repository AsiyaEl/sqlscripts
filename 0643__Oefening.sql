use aptunes;

delimiter $$
create procedure CreateAndReleaseAlbum(in titel varchar(100), in bands_Id int)
begin
start transaction;
insert into Albums (Titel)
values(titel);
-- hier mogelijk dat iemand anders een andere instructie runt, vb programma die een website aanbiedt en gebruikers tegelijkertijd database gebruiken --
-- hier zal daarom de last_insert niet over de album gaan, maar het recentste automatische gegenereerde Id gebruiken, maar als iemand de laatste recentste Id insert dan --
-- gaat dit niet meer kloppen --
-- om er iets aan te doen zorgen we ervoor dat de twee inserts samenhoren en dat doen we door een transactie op onze procedure uit te voeren.
insert into Albumreleases (Bands_Id, Albums_Id)
values(bands_Id,last_insert_id()); -- last_insert betekent nieuwe data uit Albums_Id --
commit;
end$$
delimiter ;
