use aptunes;
-- toont alle geldige combinaties van liedjestitels en genres --

select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id
where Naam = 'Rock';
