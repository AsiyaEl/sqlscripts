use aptunes;
drop procedure if exists MockAlbumRelease;
delimiter  $$
create procedure MockAlbumRelease()
begin
declare numberOfAlbums int default 0;
declare numberOfBands int default 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;
select count(*) into numberOfBands from Bands;
select count(*) into numberOfAlbums from Albums;
set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId = FLOOR(RAND() * numberOfBands) + 1;
if (randomBandId, randomAlbumId) not in (select distinct * from Albumreleases) then insert into Albumreleases(Bands_Id,Albums_Id)
values(randomBandId,randomAlbumId);
end if;

end $$ 

delimiter ;