use aptunes;
drop procedure if exists DemonstrateHandlerOrder;
delimiter $$
create procedure DemonstrateHandlerOrder()
begin
declare randomValue tinyint default 0;
declare continue handler for sqlstate '45002'
begin
select 'State 45002 opgevangen. Geen probleem';
end;
declare continue handler for sqlexception
begin
select 'Een algemene fout opgevangen.';
end;
set randomValue = floor(rand() * 3) +1;
if randomValue = 1 then
signal sqlstate '45001';
elseif randomValue = 2 then
signal sqlstate '45002';
else
signal sqlstate '45003';
end if;
end$$
delimiter ;