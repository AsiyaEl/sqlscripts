use ModernWays;

select STudenten.Id as 'Id'
from Studenten inner join Evaluaties
on Studenten.Id = Evaluaties.Studenten_Id
group by Studenten.Id 
having avg(Cijfer) >
(select avg(Cijfer) from Evaluaties);