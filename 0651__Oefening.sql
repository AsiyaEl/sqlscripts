use aptunes;
delimiter $$
create procedure GetAlbumDuration(IN album int, out totalDuration smallint unsigned)
begin
declare songDuration tinyint unsigned default 0;
declare ok bool default false;
declare songDurationCursor cursor for select Lengte from Liedjes where Albums_Id = album;

-- we zetten een continue handler omdat we niet willen dat de functie afgelopen nadat handler wordt opgeroepen. --
-- we moeten  ok op true zetten --
declare continue handler for not found set ok = True;
set totalDuration = 0;
-- we gaan een loop schrijven en een naam fetchloop geven --
open songDurationCursor; -- open de cursor --
fetchloop: loop
fetch songDurationCursor into songDuration;
if ok = True then 
leave fetchloop;
end if;
set totalDuration = totalDuration + songDuration;
end loop;
close songDurationCursor; -- sluit de cursor --

-- cursor is om aan de huidige rij te kunnen , we moeten een fetch doen om die in een variabele te steken --

-- dit is niet voldoende om het te laten werken, want de loop blijft hierbij draaien tot het een not found bereikt. --
-- not found betekent dat er geen data meer beschikbaar is. --
-- dus we moeten een handler ook declareren. --


end$$
delimiter ;