use aptunes;
drop procedure if exists MockAlbumReleaseWithSuccess;
delimiter  $$
create procedure MockAlbumReleaseWithSuccess(OUT success bool)
begin
declare numberOfAlbums int default 0;
declare numberOfBands int default 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;
select count(*) into numberOfBands from Bands;
select count(*) into numberOfAlbums from Albums;
set randomBandId = FLOOR(RAND() * numberOfBands) + 1;
set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;

if (randomBandId, randomAlbumId) not in (select distinct * from Albumreleases) then 
insert into Albumreleases(Bands_Id,Albums_Id)
values(randomBandId,randomAlbumId);
set success = 1;
else 
set success = 0;
end if;
end $$ 

delimiter ;