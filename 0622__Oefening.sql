use aptunes;
-- je voert eerste deel uit om te zien welke getal je moet nemen als aantal voor prefix lengte --
-- select count(distinct left(Voornaam,9)) from Muzikanten; --
create index VoornaamFamilienaamIdx on Muzikanten(Voornaam(9),Familienaam(9));