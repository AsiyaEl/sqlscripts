use aptunes;
drop  procedure if exists DangerousInsertAlbumreleases;

delimiter $$
create procedure DangerousInsertAlbumreleases()
begin
declare randomValue tinyint default 0;
-- 3 willekeurige albumreleases aanmaken --
declare numberofAlbums int default 0;
declare numberofBands int default 0;
declare randomAlbumId1 int default 0;
declare randomBandId1 int default 0;
declare randomAlbumId2 int default 0;
declare randomBandId2 int default 0;
declare randomAlbumId3 int default 0;
declare randomBandId3 int default 0;
-- exception handler opstellen om fouten op te vangen --
 
declare exit handler for sqlexception
begin
 rollback;
 select 'Nieuwe releases konden niet worden toegevoegd.';
end;

select count(*) into numberOfAlbums from Albums;
select count(*) into numberOfBands from Bands;
set randomAlbumId1 = floor(rand() * numberOfAlbums) +1;
set randomAlbumId2 = floor(rand() * numberOfAlbums) +1;
set randomAlbumId3 = floor(rand() * numberOfAlbums) +1;
set randomBandId1 = floor(rand() * numberOfBands) +1;
set randomBandId2 = floor(rand() * numberOfBands) +1;
set randomBandId3 = floor(rand() * numberOfBands) +1;

-- rollback betekent "transactie wordt nu teruggedraaid". DWZ. je hebt een bepaalde checkpoint gemaakt en we gaan terug naar die checkpoint --> waar staat start transaction. --

start transaction; -- hier begint de transactie --
-- nu hebben we de willekeurige waarde en kunnen we een insert doen --
insert into albumreleases(Bands_Id,Albums_Id)
values
(randomBandId1,randomAlbumId1),
(randomBandId2,randomAlbumId2);
-- nu op basis van een randomValue bepalen of er nog een foutmelding moet gegeven worden --
set randomValue = floor(rand() * 3) + 1;
if randomValue = 1 then
signal sqlstate '45000';
end if;
-- insert om het derde album uit te brengen --
insert into albumreleases(Bands_Id,Albums_Id)
values
(randomBandId3,randomAlbumId3);
commit;       -- hier eindigt de transactie --
-- ondanks deze regeling kan er toch nog mislopen, dus zullen we nog een extra Handler toevoegen --

end$$
delimiter ;