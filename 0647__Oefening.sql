use aptunes;
drop procedure if exists MockAlbumReleases
delimiter $$
create procedure MockAlbumReleases(IN extraReleases INT)
begin
declare counter int default 0;
declare success bool;
repeat
call MockAlbumReleaseWithSuccess(success);
if success = 1 then
set counter = counter + 1;
end if;
until counter = extraReleases
end repeat;
end $$
delimiter ;