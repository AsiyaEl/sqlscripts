use AsiyaElmayouri;
SET SQL_SAFE_UPDATES = 0;
UPDATE films SET Commentaar = 'Vanaf een bepaalde leeftijd';
SET SQL_SAFE_UPDATES = 1;

-- Hierbij had ik verandering aangebracht bij de kolom Commentaar. Ik veranderde de commentaar ' Het bekijken waard' naar 'Vanaf een bepaalde leeftijd'
-- Ik vond de verwijzing om de leeftijd te checken belangrijker. Deze geeft informatie aan de klant over de inhoud van de film.