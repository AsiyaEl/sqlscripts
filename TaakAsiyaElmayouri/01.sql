use AsiyaElmayouri;
CREATE TABLE films (
Titel VARCHAR (50) CHAR SET utf8mb4 NOT NULL,
Genre VARCHAR (50)CHAR SET utf8mb4 NOT NULL,
Duur  TIME NOT NULL,
Leeftijdscategorie TINYINT UNSIGNED NOT NULL,
Favoriet VARCHAR(50) CHAR SET utf8mb4  NULL,
Commentaar VARCHAR (1000) CHAR SET utf8mb4
);

-- Ik heb gekozen om over het onderwerp films een tabel te maken. 
-- Ik koos om voor Titel, Genre en Commentaar een VARCHAR te nemen, omdat ik meer dan 1 karakter dien in te voegen als naam van deze kolommen.
-- Daarbij koos ik voor not null, omdat NOT NULL ervoor zorgt dat het veld verplicht wordt ingevuld. Deze zal het ook aangeven indien het niet is ingevuld.
-- ik vond dat het verplicht moest worden voor Titel en Genre en Duur en Leeftijdscategorie, omdat je dan op deze manier kunt selecteren welke film je wilt zien,
-- voor welke leeftijd en welke genre film je wenst te zien. Ook het duur vond ik belangrijk. Wilt de klant een film zien met een duur van ongeveer 1 uur of 2 uur etc...alter

-- VOOR LEEFTIJDSCATEGORIE koos ik voor TinyInt unsigned (unsigned omdat ik leeftijden wil laten ingeven) niemand is onder de nul jaar en ik koos voor TinyInt : dat was
-- de kleinste int die ik kon nemen, gezien niemand ouder kan worden dan 100 jaar en met behoud van goede zicht en goed gehoor om een film te bekijken. Misschien dat er wonderen bestaan.
-- ik heb een Char set gevolgd door utf8mb4 gekozen omdat ik te maken heb met films die ik nog zou kunnen toevoegen in alle genre, ook arabische of japanse films of dergelijke
-- ik wou er rekening mee houden in de toekomst.
