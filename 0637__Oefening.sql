-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
USE ModernWays;
DROP TABLE IF EXISTS Personen;
CREATE TABLE Personen (
    Voornaam VARCHAR(255) NOT NULL,
    Familienaam VARCHAR(255) NOT NULL
);
-- Met een subquery die een tabel materialiseert, kunnen we we de tabel Personen meteen invullen: --
-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery

INSERT INTO Personen (Voornaam, Familienaam)
SELECT DISTINCT Voornaam, Familienaam FROM Boeken;
   
-- Je kan verifiëren dat de dubbele eruit zijn. Gebruik de ORDER BY clausule om het verifiëren te vergemakkelijken: --

SELECT Voornaam, Familienaam FROM Personen
ORDER BY Voornaam, Familienaam;

-- We hebben nu een nieuwe tabel Personen. Maar met slechts twee kolommen. --
-- Met de alter instructie voegen we de andere kolommen toe, evenals de primary key: --

ALTER TABLE Personen ADD (
   Id INT AUTO_INCREMENT PRIMARY KEY,
   AanspreekTitel VARCHAR(30) NULL,
   Straat VARCHAR(80) NULL,
   Huisnummer VARCHAR (5) NULL,
   Stad VARCHAR (50) NULL,
   Commentaar VARCHAR (100) NULL,
   Biografie VARCHAR(400) NULL);

-- we testen uit: --
  
  SELECT * FROM Personen ORDER BY Familienaam, Voornaam;
  -- De foreign key kolom in de tabel Boeken invullen --
-- ALTER TABLE Boeken ADD Personen_Id INT NULL; WE LATEN DEZE WEGVALLEN EN VOEGEN ZE LATER TERUG TOE --


-- voorlopig niet verplicht maken --
alter table Boeken add Personen_Id int null;

-- De kolom Voornaam en Familienaam want we hebben ze net gekopieerd. 
-- We kunnen bekijken hoe we de twee tabellen zullen linken op basis van deze twee kolommen: --
SELECT Boeken.Voornaam,
   Boeken.Familienaam,
   Boeken.Personen_Id,
   Personen.Voornaam,
   Personen.Familienaam,
   Personen.Id
FROM Boeken CROSS JOIN Personen
WHERE Boeken.Voornaam = Personen.Voornaam AND
    Boeken.Familienaam = Personen.Familienaam;

-- In de set clausule van de update instructie bepalen we dat de waarde van de Id van Personen in de kolom Personen_Id van Boeken gekopieerd moet worden. --
-- Met de from clause geven we aan dat de Id waarde uit de Personen tabel moet worden gehaald. --

SET SQL_SAFE_UPDATES = 0;
UPDATE Boeken CROSS JOIN Personen
    SET Boeken.Personen_Id = Personen.Id
WHERE Boeken.Voornaam = Personen.Voornaam AND
    Boeken.Familienaam = Personen.Familienaam;
    SET SQL_SAFE_UPDATES = 1;

 -- foreign key verplicht maken
ALTER TABLE Boeken CHANGE Personen_Id Personen_Id INT NOT NULL;

-- testen --
SELECT Voornaam, Familienaam, Personen_Id FROM Boeken;


-- Dubbele kolommen verwijderen uit de tabel Boeken --
 ALTER TABLE Boeken DROP COLUMN Voornaam,
    DROP COLUMN Familienaam;
    
    --   De foreign key constraint op de kolom Personen_Id toevoegen --
 -- dan de constraint toevoegen
  ALTER TABLE Boeken ADD CONSTRAINT fk_Boeken_Personen
   FOREIGN KEY(Personen_Id) REFERENCES Personen(Id);
    

   


  


